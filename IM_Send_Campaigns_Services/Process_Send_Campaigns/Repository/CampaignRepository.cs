﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Process_Send_Campaigns.Repository
{
    public class CampaignRepository
    {
        public CampaignRepository()
        {
        }

        public List<Campaigns> GetCampaignInProcess()
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.Campaigns.Include(s => s.Senders).Include(s => s.ListDirects).Include(s => s.Companies).Where(x => x.Status == (int)Enums.CampaignStatus.Procesando || x.Status == (int)Enums.CampaignStatus.Test_Procesando).ToList();
                return obj;
            }
        }
        public List<Campaigns> GetCampaignInSendingProcess()
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.Campaigns.Include(s => s.Senders).Include(s => s.ListDirects).Include(s => s.Companies).Where(x => x.Status == (int)Enums.CampaignStatus.Enviando || x.Status == (int)Enums.CampaignStatus.Test_Enviando || x.Status == (int)Enums.CampaignStatus.Fallo || x.Status == (int)Enums.CampaignStatus.Test_Fallo).ToList();
                return obj;
            }
        }
        public Senders GetSenders(long senderId)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.Senders.SingleOrDefault(x => x.Id == senderId);
                return obj;
            }
        }
        public void EditCampaign(Campaigns campaign)
        {
            var campaignToUpdate = GetById(campaign.Id);
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                campaignToUpdate.Name = campaign.Name;
                campaignToUpdate.Subject = campaign.Subject;
                campaignToUpdate.MailerKey = campaign.MailerKey;
                campaignToUpdate.SenderId = campaign.SenderId;
                campaignToUpdate.HtmlContent = campaign.HtmlContent;
                campaignToUpdate.IsCode = campaign.IsCode;
                campaignToUpdate.Status = campaign.Status;
                campaignToUpdate.WasSent = campaign.WasSent;
                campaignToUpdate.TotalSent = campaign.TotalSent;
                campaignToUpdate.SentDate = campaign.SentDate;
                //campaignToUpdate.ListDirectId = campaign.ListDirectId == 0 ? null : campaign.ListDirectId;
                campaignToUpdate.TypeCampaign = campaign.TypeCampaign;
                _dbContext.Entry(campaignToUpdate).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
        }
        public Campaigns GetById(long id)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.Campaigns.Include(x => x.Companies).FirstOrDefault(c => c.Id == id);
                return obj;
            }
        }
        public List<Campaigns> GetByDate(DateTime fecha)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var f1 = new DateTime(fecha.Year, fecha.Month, fecha.Day, 0, 0, 0);
                var f2 = f1.AddDays(1).AddSeconds(-1);
                var obj = _dbContext.Campaigns.Include(x => x.Senders).Where(c => c.SentDate >= f1 && c.SentDate <= f2)
                    .ToList();
                return obj;
            }
        }
        public StatTotalEvents GetStatTotalEventByMaylerKey(string MaylerKey)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.StatTotalEvents.SingleOrDefault(x => x.MailerKey == MaylerKey);
                return obj;
            }
        }
        public ListDirects GetListDirectsById(long id)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.ListDirects.FirstOrDefault(l => l.Id == id);
                obj.RecipientDirects = null;
                return obj;
            }
        }
        public List<object> GetListRecipentsDirectsByListDirectId(long id)
        {
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                var obj = _dbContext.RecipientDirects.Where(l => l.ListDirectId == id).ToList<object>();
                return obj;
            }
        }
        public void UpdateStatTotalEvent(string mailerKey, int request)
        {
            var obj = GetStatTotalEventByMaylerKey(mailerKey);
            using (IzyMarketingEntities _dbContext = new IzyMarketingEntities())
            {
                obj.Request = request;
                _dbContext.Entry(obj).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }
        }
    }
}
