﻿
namespace Process_Send_Campaigns.Enums
{
    public enum FileStatus
    {
        Processing = 0,
        Completed = 1,
        CompletedWithErrors = 2,
        Failed = 3,
        NotVisible = 4
    }
}
