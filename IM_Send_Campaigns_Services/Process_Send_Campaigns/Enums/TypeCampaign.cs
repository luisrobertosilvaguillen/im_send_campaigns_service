﻿namespace Process_Send_Campaigns.Enums
{
    public enum TypeCampaign
    {
        Segment = 1,
        ListDirect = 2,
        All = 3
    }
}