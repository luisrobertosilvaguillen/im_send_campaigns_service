﻿
namespace Process_Send_Campaigns.Enums
{
    public enum CampaignStatus
    {
        Draft = 0,
        Procesando = 1,
        Enviando = 2,
        Enviada = 3,
        Fallo = 4,
        Test_Procesando = 5,
        Test_Enviando = 6,
        Test_Enviada = 7,
        Test_Fallo = 8
    }
}
