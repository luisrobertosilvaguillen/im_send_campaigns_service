﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Process_Send_Campaigns.Enums;
using Process_Send_Campaigns.Util;
using LINQtoCSV;
using HtmlAgilityPack;
using System.Net.Mail;
using System.Collections;
using System.Net;
using System.Net.Mime;
using Process_Send_Campaigns.Repository;
using System.Threading;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace Process_Send_Campaigns
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }
        private System.Windows.Forms.Timer timer_cron = new System.Windows.Forms.Timer();
        private CampaignRepository _repo = new CampaignRepository();
        private void FrmMain_Load(object sender, EventArgs e)
        {
            ChekPreviousCamps();
            timer_cron.Tick += new EventHandler(TimerEventProcessor);
            timer_cron.Interval = 10000 * 2; // 60000
            timer_cron.Start();
        }
        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            try //
            {
                List<Campaigns> campaigns = _repo.GetCampaignInProcess();
                foreach (Campaigns camp in campaigns)
                {
                    switch (camp.TypeCampaign)
                    {
                        //case TypeCampaign.All:
                        //    break;
                        case (int)TypeCampaign.ListDirect:
                            try
                            {
                                bool isTest = camp.Status == (int)CampaignStatus.Test_Procesando;
                                if (isTest || !camp.IsSchedule || (camp.IsSchedule && camp.SentDate <= DateTime.Now))
                                {
                                    camp.Status = !isTest ? (int)CampaignStatus.Enviando : (int)CampaignStatus.Test_Enviando;
                                    if (!camp.IsSchedule)
                                        camp.SentDate = DateTime.Now;
                                    _repo.EditCampaign(camp);
                                    var task = new Thread(() => start_send_directlist(camp.Id, isTest));
                                    task.Start();
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteLogError("\n [Mensaje] " + String.Format("Un error ha ocurrido obteniendo todos los list direct campaña id: {0}, detalle del error:{1}", camp.Id, ex.Message) + "\n [InnerException] " + (ex.InnerException != null ? ex.InnerException.ToString() : ""));

                                camp.Status = camp.Status == (int)CampaignStatus.Enviando ? (int)CampaignStatus.Fallo : (int)CampaignStatus.Test_Fallo;
                                _repo.EditCampaign(camp);
                                return;
                            }
                            break;
                            //case TypeCampaign.Segment:
                            //            break;
                    }
                }

            }
            catch (Exception ex)
            {
                WriteLogError("\n [Mensaje] " + ex.Message + "\n [InnerException] " + (ex.InnerException != null ? ex.InnerException.ToString() : ""));
            }
        }
        /// <summary>
        /// Verifica las campañas que tienen un estatus = Enviando O Test_Enviando
        /// Ya que si existe es porque esta app se ha cerrado subitamente antes de pasarlas a un estatus = Enviada o Test_Enviada
        /// Si consigue campañas no enviadas buscará el log de 'Log Emails-Sent' correspondiente a las mismas para saber en que parte del
        /// envío iba cuando la aplicación se cerró y asi poder continuar el envío a los emails faltantes.
        /// </summary>
        private void ChekPreviousCamps()
        {
            List<Campaigns> campaigns = _repo.GetCampaignInSendingProcess();
            foreach (var camp in campaigns)
            {
                bool isTest = camp.Status == (int)CampaignStatus.Test_Enviando || camp.Status == (int)CampaignStatus.Test_Fallo;
                string fecha = DateTime.Now.ToString("dd-MM-yyyy");
                string logPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + camp.SentDate.Value.ToString("dd-MM-yyyy") + "\\" + camp.Id + "\\";
                if (Directory.Exists(logPath))
                {
                    string path = logPath + "Log Emails-Sent.dat";
                    string line = File.ReadLines(path).Last();
                    int linnum = 0;
                    bool isvalid = int.TryParse(line.Split(' ')[1], out linnum);
                    if (isvalid)
                    {
                        var task = new Thread(() => start_send_directlist(camp.Id, isTest, (linnum + 1)));
                        task.Start();
                    }
                }
                else
                {
                    var task = new Thread(() => start_send_directlist(camp.Id, isTest, 1));
                    task.Start();
                }
            }
        }
        #region Metodos Logs
        public void WriteLogError(string data)
        {
            string fecha = DateTime.Now.ToString("dd-MM-yyyy");
            string hora = DateTime.Now.ToString("[HH:mm:ss]");

            string logErrorPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + fecha + "\\";

            if (!Directory.Exists(logErrorPath))
            {
                DirectoryInfo newDirectory = Directory.CreateDirectory(logErrorPath);
            }

            using (StreamWriter writer = new StreamWriter(logErrorPath + "Log Error.dat", true))
            {
                writer.WriteLine(hora + ": " + data);
                writer.Close();
            }
        }
        public void WriteLogSending(string data, string campaignId, string fecha = "")
        {
            if (fecha == "")
                fecha = DateTime.Now.ToString("dd-MM-yyyy");
            string hora = DateTime.Now.ToString("[HH:mm:ss]");

            string logPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + fecha + "\\" + campaignId + "\\";

            if (!Directory.Exists(logPath))
            {
                DirectoryInfo newDirectory = Directory.CreateDirectory(logPath);
            }

            using (StreamWriter writer = new StreamWriter(logPath + "Log Emails-Sent.dat", true))
            {
                writer.WriteLine(hora + ": " + data);
                writer.Close();
            }
        }
        public void WriteLogSendingFails(string data, string campaignId)
        {
            string fecha = DateTime.Now.ToString("dd-MM-yyyy");
            string hora = DateTime.Now.ToString("[HH:mm:ss]");

            string logPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + fecha + "\\" + campaignId + "\\";

            if (!Directory.Exists(logPath))
            {
                DirectoryInfo newDirectory = Directory.CreateDirectory(logPath);
            }

            using (StreamWriter writer = new StreamWriter(logPath + "Log Sending Fails.dat", true))
            {
                writer.WriteLine(hora + ": " + data);
                writer.Close();
            }
        }
        #endregion
        #region Metodos Procesamiento
        private void start_send_directlist(long campaignId, bool isTest, int indexLogBreak = -1)
        {
            Campaigns camp = _repo.GetById(campaignId);
            long ListDirectId = isTest ? (long)camp.ListDirectTestId : (long)camp.ListDirectId;
            var listDirect = _repo.GetListDirectsById(ListDirectId);
            List<ListDirectHeader> listHeaders = GetListHeaderFromListDirect(listDirect);
            //camp.TotalSent = listDirect.RecipientDirects.Count();
            List<object> recipients = new List<object>();
            //if ((camp.TypeCampaign == (int)TypeCampaign.ListDirect && !isTest) || (camp.TypeCampaign == (int)TypeCampaign.ListDirect && isByList))
            if ((camp.TypeCampaign == (int)TypeCampaign.ListDirect))
            {
                var listRecipentsDirect = ProcessingDirectFile(listHeaders.ToArray(), listDirect, true);
                recipients = listRecipentsDirect.ToList<object>();
            }
            //else
            //{
            //    recipients = ((IEnumerable)emailObj).Cast<object>().ToList();
            //}
            SendingCampaing_Smtp(recipients, camp, isTest, indexLogBreak);
        }
        private void SendingCampaing_Smtp(List<object> recipients, Campaigns campaign, bool isTest, int indexLogBreak)
        {
            try
            {

                bool sendCampaignEnable = Convert.ToBoolean(ConfigurationManager.AppSettings["SendCampaignEnable"]);
                string Subject = isTest ? "Preview - " + campaign.Subject : campaign.Subject;
                var nameList = string.Empty;
                var listName = string.Empty;
                int stepBreakLog = int.Parse(ConfigurationManager.AppSettings["StepBreakLog"]);
                int cont = 1;
                Senders senders = _repo.GetSenders(campaign.SenderId.Value);
                if (!isTest)
                    _repo.UpdateStatTotalEvent(campaign.MailerKey, recipients.Count);
                foreach (var recipent in recipients)
                {
                    string Email = ((dynamic)recipent).Email;
                    try
                    {
                        if ((indexLogBreak > 0 && cont >= indexLogBreak) || indexLogBreak == -1)
                        {
                            string body = "";
                            string xmstpapiJson = XsmtpapiHeaderAsJson(campaign, isTest, recipent, true, ref body);
                            var client = new SmtpClient
                            {
                                Port = 587,
                                Host = "smtp.sendgrid.net",
                                Timeout = 10000,
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = false
                            };
                            client.Credentials = new NetworkCredential(campaign.Companies.MailerUser, campaign.Companies.MailerPassword);
                            var htmltotext = StripHTML(body);
                            var mail = new MailMessage
                            {
                                From = new MailAddress(senders.Name + " <" + senders.Email + ">"),
                                Subject = Subject,
                                Body = htmltotext,
                            };
                            mail.SubjectEncoding = System.Text.Encoding.UTF8;
                            mail.Headers.Add("X-SMTPAPI", xmstpapiJson);
                            ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                            AlternateView alternate = AlternateView.CreateAlternateViewFromString(body, mimeType);
                            mail.AlternateViews.Add(alternate);
                            mail.BodyEncoding = Encoding.UTF8;
                            mail.To.Add(new MailAddress(Email));
                            //mail.To.Add(new MailAddress("lsilva@dnsconsultores.com"));
                            //client.Send(mail);
                            mail.Dispose();
                            client.Dispose();
                            if (((cont > 1 && (cont % stepBreakLog) == 0) || cont == recipients.Count || cont == 1) && !isTest)
                            {
                                WriteLogSending(cont.ToString(), campaign.Id.ToString(), indexLogBreak > 0 ? campaign.SentDate.Value.ToString("dd-MM-yyyy") : "");
                            }
                        }
                        cont++;
                    }
                    catch (Exception e)
                    {
                        WriteLogSendingFails(Email + " - " + e.Message, campaign.Id.ToString());
                    }
                }
                if (!isTest)
                {
                    campaign.WasSent = true;
                    campaign.TotalSent = cont - 1;
                }
                campaign.Status = (campaign.Status == (int)CampaignStatus.Enviando || campaign.Status == (int)CampaignStatus.Fallo) ? (int)CampaignStatus.Enviada : (int)CampaignStatus.Test_Enviada;
                _repo.EditCampaign(campaign);
            }
            catch (Exception ex)
            {
                WriteLogError("\n [Mensaje] " + ex.Message + "\n [InnerException] " + (ex.InnerException != null ? ex.InnerException.ToString() : ""));

            }
        }
        private void UpdateTotalsAndValuesListDirect(ListDirectHeader[] listDirectHeaders, ListDirects listDirectNew)
        {
            var listDirect = _repo.GetListDirectsById(listDirectNew.Id);
            listDirect.Status = listDirectNew.Status;
            listDirect.TotalRecipients = listDirectNew.TotalRecipients;
            listDirect.FilePathError = listDirectNew.FilePathError;
            var emailConst = ConfigurationManager.AppSettings["ColumnEmail"];
            foreach (var obj in listDirectHeaders)
            {
                listDirect.NumberEmail = obj.NameTag == emailConst ? obj.NumberColumn : listDirect.NumberEmail;
                listDirect.NameTag1 = obj.NameTag == "tag1" ? obj.NameColumn : listDirect.NameTag1;
                listDirect.NumberTag1 = obj.NameTag == "tag1" ? obj.NumberColumn : listDirect.NumberTag1;
                listDirect.NameTag2 = obj.NameTag == "tag2" ? obj.NameColumn : listDirect.NameTag2;
                listDirect.NumberTag2 = obj.NameTag == "tag2" ? obj.NumberColumn : listDirect.NumberTag2;
                listDirect.NameTag3 = obj.NameTag == "tag3" ? obj.NameColumn : listDirect.NameTag3;
                listDirect.NumberTag3 = obj.NameTag == "tag3" ? obj.NumberColumn : listDirect.NumberTag3;
                listDirect.NameTag4 = obj.NameTag == "tag4" ? obj.NameColumn : listDirect.NameTag4;
                listDirect.NumberTag4 = obj.NameTag == "tag4" ? obj.NumberColumn : listDirect.NumberTag4;
                listDirect.NameTag5 = obj.NameTag == "tag5" ? obj.NameColumn : listDirect.NameTag5;
                listDirect.NumberTag5 = obj.NameTag == "tag5" ? obj.NumberColumn : listDirect.NumberTag5;
            }
            //_repo.EditCampaign();
            //using (IzyMarketingEntities _db = new IzyMarketingEntities())
            //{
            //    _db.Entry(listDirect).State = EntityState.Modified;
            //    _db.SaveChanges();
            //}
        }
        private bool ValidateFiledRecipientsDirect(int? numbertag, MyDataRow row)
        {
            if (numbertag != null)
            {
                if (!Util.Util.IsValidString(row[(int)numbertag].Value))
                    return false;
            }
            return true;
        }
        private string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {
                return source;
            }
        }
        private string XsmtpapiHeaderAsJson(Campaigns campaign, bool isTest, dynamic recipent, bool withTags, ref string body)
        {
            var campaignName = campaign.Name;
            var header = new SendGrid.SmtpApi.Header();
            var uniqueArgs = new Dictionary<string, string>
            {
                {
                    "campaign_id", (campaign.Id.ToString() +(isTest ? "_test" : ""))
                },
                {
                    "campaignName", campaignName
                }
            };
            var template = campaign.HtmlContent;
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(template);

            body = htmlDoc.DocumentNode.SelectSingleNode("//body").InnerHtml;
            htmlDoc.DocumentNode.SelectSingleNode("//body").InnerHtml = "<% body %>";

            header.AddUniqueArgs(uniqueArgs);
            if (withTags)
            {
                string webmail_vars = string.Empty;

                if (recipent.NameTag1 != null && recipent.NameTag1 != string.Empty)
                {
                    header.AddSubstitution("[" + recipent.NameTag1 + "]", new List<String> { recipent.Tag1 });
                    webmail_vars += "&" + recipent.NameTag1 + "=" + recipent.Tag1;
                }
                if (recipent.NameTag2 != null && recipent.NameTag2 != string.Empty)
                {
                    header.AddSubstitution("[" + recipent.NameTag2 + "]", new List<String> { recipent.Tag2 });
                    webmail_vars += "&" + recipent.NameTag2 + "=" + recipent.Tag2;
                }
                if (recipent.NameTag3 != null && recipent.NameTag3 != string.Empty)
                {
                    header.AddSubstitution("[" + recipent.NameTag3 + "]", new List<String> { recipent.Tag3 });
                    webmail_vars += "&" + recipent.NameTag3 + "=" + recipent.Tag3;
                }
                if (recipent.NameTag4 != null && recipent.NameTag4 != string.Empty)
                {
                    header.AddSubstitution("[" + recipent.NameTag4 + "]", new List<String> { recipent.Tag4 });
                    webmail_vars += "&" + recipent.NameTag4 + "=" + recipent.Tag4;
                }
                if (recipent.NameTag5 != null && recipent.NameTag5 != string.Empty)
                {
                    header.AddSubstitution("[" + recipent.NameTag5 + "]", new List<String> { recipent.Tag5 });
                    webmail_vars += "&" + recipent.NameTag5 + "=" + recipent.Tag5;
                }
                webmail_vars += "&email=" + recipent.Email;
                webmail_vars += "&Email=" + recipent.Email;
                string weblink = ConfigurationManager.AppSettings["WebLinkUrl"] + "ku2Ffi1RzIaAtPPVvugghw=" + Encrypt("id=" + campaign.Id.ToString() + webmail_vars);

                header.AddSubstitution("[weblink]", new List<String> { weblink });
                header.AddSubstitution("[Weblink]", new List<String> { weblink });
            }
            header.AddSubstitution("[email]", new List<String> { recipent.Email });
            header.AddSubstitution("[Email]", new List<String> { recipent.Email });
            header.AddFilterSetting("clicktrack", new List<string> { "enable" }, "1");
            header.AddFilterSetting("template", new List<string> { "enable" }, "1");
            header.AddFilterSetting("template", new List<string> { "text/html" }, htmlDoc.DocumentNode.InnerHtml);
            header.AddFilterSetting("subscriptiontrack", new List<string> { "enable" }, "1");
            header.AddFilterSetting("subscriptiontrack", new List<string> { "replace" }, "[unsubscribe]");
            return header.JsonString();
        }
        private string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(ConfigurationManager.AppSettings["PasswordHash"], Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["SaltKey"])).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["VIKey"]));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }
        private List<ListDirectHeader> GetListHeaderFromListDirect(ListDirects listDirect)
        {
            var listHeaders = new List<ListDirectHeader>();
            var listDirectHeader = new ListDirectHeader();
            listDirectHeader.NameTag = ConfigurationManager.AppSettings["ColumnEmail"];
            listDirectHeader.NameColumn = ConfigurationManager.AppSettings["ColumnEmail"];
            listDirectHeader.NumberColumn = listDirect.NumberEmail;
            listHeaders.Add(listDirectHeader);
            if (!string.IsNullOrEmpty(listDirect.NameTag1))
            {
                var listDirectHeader1 = new ListDirectHeader();
                listDirectHeader1.NameTag = "tag1";
                listDirectHeader1.NameColumn = listDirect.NameTag1;
                listDirectHeader1.NumberColumn = listDirect.NumberTag1;
                listHeaders.Add(listDirectHeader1);
            }
            if (!string.IsNullOrEmpty(listDirect.NameTag2))
            {
                var listDirectHeader2 = new ListDirectHeader();
                listDirectHeader2.NameTag = "tag2";
                listDirectHeader2.NameColumn = listDirect.NameTag2;
                listDirectHeader2.NumberColumn = listDirect.NumberTag2;
                listHeaders.Add(listDirectHeader2);
            }
            if (!string.IsNullOrEmpty(listDirect.NameTag3))
            {
                var listDirectHeader3 = new ListDirectHeader();
                listDirectHeader3.NameTag = "tag3";
                listDirectHeader3.NameColumn = listDirect.NameTag3;
                listDirectHeader3.NumberColumn = listDirect.NumberTag3;
                listHeaders.Add(listDirectHeader3);
            }
            if (!string.IsNullOrEmpty(listDirect.NameTag4))
            {
                var listDirectHeader4 = new ListDirectHeader();
                listDirectHeader4.NameTag = "tag4";
                listDirectHeader4.NameColumn = listDirect.NameTag4;
                listDirectHeader4.NumberColumn = listDirect.NumberTag4;
                listHeaders.Add(listDirectHeader4);
            }
            if (!string.IsNullOrEmpty(listDirect.NameTag5))
            {
                var listDirectHeader5 = new ListDirectHeader();
                listDirectHeader5.NameTag = "tag5";
                listDirectHeader5.NameColumn = listDirect.NameTag5;
                listDirectHeader5.NumberColumn = listDirect.NumberTag5;
                listHeaders.Add(listDirectHeader5);
            }

            return listHeaders;
        }
        private List<RecipientDirects> ProcessingDirectFile(ListDirectHeader[] listDirectHeaders, ListDirects listDirect, bool isSendCampaign)
        {
            var emailConst = ConfigurationManager.AppSettings["ColumnEmail"];
            var csvContext = new CsvContext();
            var fileDescription = new CsvFileDescription
            {
                SeparatorChar = Convert.ToChar(listDirect.Delimiter), // default is ','
                FirstLineHasColumnNames = false,
                EnforceCsvColumnAttribute = false, // default is false
                FileCultureName = "en-US" // default is the current culture
            };
            int columnNumberEmail = listDirectHeaders.Where(e => string.Equals(e.NameTag, emailConst, StringComparison.OrdinalIgnoreCase)).Select(s => s.NumberColumn).FirstOrDefault();
            //if (columnNumberEmail == -1)
            //{
            //    using (IzyMarketingEntities _db = new IzyMarketingEntities())
            //    {
            //        listDirect.Status = (int)Enums.FileStatus.Failed;
            //        _db.Entry(listDirect).State = EntityState.Modified;
            //        _db.SaveChanges();
            //    }
            //    return null;
            //}
            var rows = csvContext.Read<MyDataRow>(listDirect.FilePath, fileDescription);
            var recipientDirectToAdd = new List<RecipientDirects>();
            var errorRowList = new List<string>();
            // ReSharper disable once PossibleMultipleEnumeration
            var numberTag1 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag1"))?.NumberColumn;
            var nameTag1 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag1"))?.NameColumn;
            var numberTag2 = listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag2")?.NumberColumn;
            var nameTag2 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag2"))?.NameColumn;
            var numberTag3 = listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag3")?.NumberColumn;
            var nameTag3 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag3"))?.NameColumn;
            var numberTag4 = listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag4")?.NumberColumn;
            var nameTag4 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag4"))?.NameColumn;
            var numberTag5 = listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag5")?.NumberColumn;
            var nameTag5 = (listDirectHeaders.FirstOrDefault(t => t.NameTag == "tag5"))?.NameColumn;
            foreach (var row in rows)
            {
                try
                {
                    var emailRow = row[columnNumberEmail].Value?.Trim();
                    if (!Util.Util.IsValidString(row[columnNumberEmail].Value))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    // ReSharper disable once PossibleMultipleEnumeration
                    if (!Util.Util.IsValidEmail(emailRow) || row == rows.First())
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }

                    if (!ValidateFiledRecipientsDirect(numberTag1, row))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    if (!ValidateFiledRecipientsDirect(numberTag2, row))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    if (!ValidateFiledRecipientsDirect(numberTag3, row))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    if (!ValidateFiledRecipientsDirect(numberTag4, row))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    if (!ValidateFiledRecipientsDirect(numberTag5, row))
                    {
                        // crear archivo de error
                        var errorRow = new List<string>();
                        row.ForEach(dr =>
                        {
                            errorRow.Add(dr.Value);
                        });
                        errorRowList.Add(string.Join(",", errorRow));

                        continue;
                    }
                    var recipientDirect = new RecipientDirects()
                    {
                        ListDirectId = listDirect.Id,
                        CreatedDate = DateTime.Now,
                        Email = emailRow.ToLower(),
                        Tag1 = numberTag1 != null ? row[(int)numberTag1].Value : string.Empty,
                        NameTag1 = nameTag1,
                        Tag2 = numberTag2 != null ? row[(int)numberTag2].Value : string.Empty,
                        NameTag2 = nameTag2,
                        Tag3 = numberTag3 != null ? row[(int)numberTag3].Value : string.Empty,
                        NameTag3 = nameTag3,
                        Tag4 = numberTag4 != null ? row[(int)numberTag4].Value : string.Empty,
                        NameTag4 = nameTag4,
                        Tag5 = numberTag5 != null ? row[(int)numberTag5].Value : string.Empty,
                        NameTag5 = nameTag5
                    };
                    recipientDirectToAdd.Add(recipientDirect);

                }
                catch (Exception)
                {
                    var errorRow = new List<string>();
                    row.ForEach(dr =>
                    {
                        errorRow.Add(dr.Value);
                        WriteLogError("\n [Mensaje] " + dr.Value);
                    });
                    errorRowList.Add(string.Join(",", errorRow));
                }
            }

            // creating the error file
            if (!isSendCampaign)
            {
                listDirect.Status = errorRowList.Count > 1
                    ? (int)Enums.FileStatus.CompletedWithErrors
                    : (int)Enums.FileStatus.Completed;
                listDirect.TotalRecipients = recipientDirectToAdd.Count;
                UpdateTotalsAndValuesListDirect(listDirectHeaders, listDirect);
            }
            //listDirect.RecipientDirects = recipientDirectToAdd;

            return recipientDirectToAdd;
        }
        internal class MyDataRow : List<DataRowItem>, IDataRow, ICloneable
        {
            public object Clone()
            {
                return MemberwiseClone();
            }
        }
        #endregion

        private void lnkIndicadores_Click(object sender, EventArgs e)
        {
            StatsTimeSend frm = new StatsTimeSend();
            frm.Show();
        }
    }
}
