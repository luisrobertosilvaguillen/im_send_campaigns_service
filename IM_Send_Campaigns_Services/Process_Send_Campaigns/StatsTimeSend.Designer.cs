﻿namespace Process_Send_Campaigns
{
    partial class StatsTimeSend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatsTimeSend));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtFecha = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grdCampaign = new System.Windows.Forms.DataGridView();
            this.Sender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.campania = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totaltiempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalenvio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCampaign)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtFecha);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1256, 46);
            this.panel1.TabIndex = 2;
            // 
            // txtFecha
            // 
            this.txtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtFecha.Location = new System.Drawing.Point(88, 17);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(123, 24);
            this.txtFecha.TabIndex = 3;
            this.txtFecha.ValueChanged += new System.EventHandler(this.txtFecha_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fecha";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.grdCampaign);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1256, 435);
            this.panel2.TabIndex = 3;
            // 
            // grdCampaign
            // 
            this.grdCampaign.AllowUserToAddRows = false;
            this.grdCampaign.AllowUserToDeleteRows = false;
            this.grdCampaign.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdCampaign.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCampaign.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sender,
            this.campania,
            this.status,
            this.email1,
            this.email2,
            this.totaltiempo,
            this.totalenvio});
            this.grdCampaign.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdCampaign.Location = new System.Drawing.Point(0, 19);
            this.grdCampaign.Name = "grdCampaign";
            this.grdCampaign.RowHeadersVisible = false;
            this.grdCampaign.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCampaign.Size = new System.Drawing.Size(1256, 416);
            this.grdCampaign.TabIndex = 0;
            // 
            // Sender
            // 
            this.Sender.DataPropertyName = "sender";
            this.Sender.HeaderText = "Sender";
            this.Sender.Name = "Sender";
            this.Sender.ReadOnly = true;
            this.Sender.Width = 200;
            // 
            // campania
            // 
            this.campania.DataPropertyName = "name";
            this.campania.HeaderText = "Campaña";
            this.campania.Name = "campania";
            this.campania.ReadOnly = true;
            this.campania.Width = 310;
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Estatus";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Width = 150;
            // 
            // email1
            // 
            this.email1.HeaderText = "Fecha 1er Email";
            this.email1.Name = "email1";
            this.email1.ReadOnly = true;
            this.email1.Width = 130;
            // 
            // email2
            // 
            this.email2.HeaderText = "Fecha Ult. Email";
            this.email2.Name = "email2";
            this.email2.ReadOnly = true;
            this.email2.Width = 130;
            // 
            // totaltiempo
            // 
            this.totaltiempo.HeaderText = "Duracion de Envio";
            this.totaltiempo.Name = "totaltiempo";
            this.totaltiempo.ReadOnly = true;
            this.totaltiempo.Width = 150;
            // 
            // totalenvio
            // 
            this.totalenvio.HeaderText = "Emails Contabilizados";
            this.totalenvio.Name = "totalenvio";
            this.totalenvio.ReadOnly = true;
            this.totalenvio.Width = 180;
            // 
            // StatsTimeSend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1256, 481);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StatsTimeSend";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Indicadores de Tiempo de Envio";
            this.Load += new System.EventHandler(this.StatsTimeSend_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCampaign)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker txtFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView grdCampaign;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sender;
        private System.Windows.Forms.DataGridViewTextBoxColumn campania;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn email1;
        private System.Windows.Forms.DataGridViewTextBoxColumn email2;
        private System.Windows.Forms.DataGridViewTextBoxColumn totaltiempo;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalenvio;
    }
}