﻿using Process_Send_Campaigns.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Process_Send_Campaigns
{
    public partial class StatsTimeSend : Form
    {
        public StatsTimeSend()
        {
            InitializeComponent();
        }
        private CampaignRepository _repo = new CampaignRepository();
        private void LoadGrid()
        {
            var list = _repo.GetByDate(txtFecha.Value);
            List<dynamic> lista = new List<dynamic>();
            foreach (var obj in list)
            {
                dynamic reg = new ExpandoObject();
                reg.id = obj.Id;
                reg.sentdate = obj.SentDate;
                reg.name = obj.Id.ToString() + " - " + obj.Name;
                reg.sender = obj.Senders.Name;
                reg.status = (Enums.CampaignStatus)obj.Status;
                reg = SetObjtVal(reg);
                lista.Add(reg);
            }
            grdCampaign.AutoGenerateColumns = false;
            grdCampaign.DataSource = ToDataTable(lista);
        }
        private void StatsTimeSend_Load(object sender, EventArgs e)
        {
            txtFecha.Value = DateTime.Today;
            LoadGrid();
        }
        private void txtFecha_ValueChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }
        /// <summary>
        /// Extension method to convert dynamic data to a DataTable. Useful for databinding.
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A DataTable with the copied dynamic data.</returns>
        private DataTable ToDataTable(IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                dt.Columns.Add(key);
            }
            foreach (var d in data)
            {
                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
            }
            return dt;
        }
        private dynamic SetObjtVal(dynamic obj)
        {
            string logPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + ((DateTime)obj.sentdate).ToString("dd-MM-yyyy") + "\\" + ((int)obj.id).ToString() + "\\";
            if (Directory.Exists(logPath))
            {
                try
                {
                    string path = logPath + "Log Emails-Sent.dat";
                    var filelines = File.ReadLines(path);
                    string first = filelines.First();
                    string last = filelines.Last();
                    int linnumf = 0;
                    int linnuml = 0;
                    string[] arrlf = first.Split(' ');
                    string[] arrll = last.Split(' ');
                    bool isvalidf = int.TryParse(arrlf[1], out linnumf);
                    bool isvalidl = int.TryParse(arrll[1], out linnuml);
                    if (isvalidf && isvalidl)
                    {
                        obj.fecha1 = arrlf[0].Replace("[", "").Replace("]:", "");
                        obj.fecha2 = arrll[0].Replace("[", "").Replace("]:", "");
                    }
                }
                catch
                {

                }
            }
            return obj;
        }
    }
}
