﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Process_Send_Campaigns.Enums;
using LumenWorks.Framework.IO.Csv;
using System.Net;

namespace Process_Send_Campaigns.Util
{
    public static class Util
    {


        public static bool IsValidEmail(string email)
        {
            if (email.ToLower().Contains('ñ'))
            {
                return false;
            }
            const string expression = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                      + "@"
                                      + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";
            try
            {

                return Regex.IsMatch(email, expression);
            }
            catch (Exception)
            {

                return false;

            }
        }
        public static bool IsValidString(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;
            const string expression = @"^[a-zA-Z0-9./\s$áéíúó@ÁÉ)(ÍÓÚñÑ,_-]+$";
            try
            {
                return Regex.IsMatch(value, expression);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ValidateEnumerable<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        public static char DelimiterValidate(string row)
        {
            int countComma = row.Count(d => d == ',');
            int countPointComma = row.Count(d => d == ';');
            return countPointComma > countComma ? ';' : ',';
        }

        public static Header[] ReadFile(string filePath, ref char delimiter)
        {
            if (delimiter <= 0) throw new ArgumentOutOfRangeException(nameof(delimiter));
            string[] onlyHeaders;
            using (var reader = new CsvReader(new StreamReader(filePath), true))
            {
                var resultHeaders = reader.GetFieldHeaders();
                var headerStr = string.Join(",", resultHeaders.ToArray());
                delimiter = DelimiterValidate(headerStr);
                var cleanHeader = headerStr[headerStr.Length - 1] == delimiter
                    ? headerStr.Remove(headerStr.Length - 1)
                    : headerStr;
                onlyHeaders = cleanHeader.Split(delimiter).ToArray();
            }
            var column = 0;
            var headers = onlyHeaders.Select(e => new Header
            {
                nameColumn = e,
                numberColumn = column++

            }).ToArray();
            return headers;
        }
    }
}
