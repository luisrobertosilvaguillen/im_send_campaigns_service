﻿namespace Process_Send_Campaigns.Util
{
    public class ListDirectHeader
    {
        public string NameColumn { get; set; }
        public int NumberColumn { get; set; }
        public string NameTag { get; set; }
    }
}
