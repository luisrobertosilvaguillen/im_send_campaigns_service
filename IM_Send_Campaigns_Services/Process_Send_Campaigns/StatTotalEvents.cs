//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Process_Send_Campaigns
{
    using System;
    using System.Collections.Generic;
    
    public partial class StatTotalEvents
    {
        public long Id { get; set; }
        public string MailerKey { get; set; }
        public int Try { get; set; }
        public int Request { get; set; }
        public int Delivered { get; set; }
        public int Opened { get; set; }
        public int OpenedUnique { get; set; }
        public int Clicked { get; set; }
        public int ClickedUnique { get; set; }
        public int Bounced { get; set; }
        public int Spam { get; set; }
        public Nullable<System.DateTime> LastDateUpdated { get; set; }
    }
}
